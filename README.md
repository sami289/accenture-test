# accenture-test

> Accenture Test created on 25-06-2019

To run the app we just need to install dependencies to get the node modules.
Then run "npm run dev' to serve the app on localhost:3000

This app uses Nuxt.js, Vuetify.js as a frontend framework and AVA for testing.

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
